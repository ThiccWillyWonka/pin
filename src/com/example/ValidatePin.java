/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example;

import java.util.Scanner;

public class ValidatePin {

    public static void main(String[] args) {
        int pin = 1111;
        Scanner sc = new Scanner(System.in);
        System.out.print("Please enter your PIN : ");
        int enteredPin = sc.nextInt();
        
        while (enteredPin != pin){
            System.out.print("Please enter the PIN again : ");
            enteredPin = sc.nextInt();
            
            
        }
        
        System.out.println("PIN correct :-). Now you have access to your account.");
    }
}
